package model

import androidx.compose.ui.geometry.Offset
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals

/**
 * Simple test that asserts proper functionality of the class ConvexHull
 * by initializing it with empty list and predefined list with 5 points.
 */
class ConvexHullTest {

    private var testConvexHull = ConvexHull(emptyList())

    private val testPoints = listOf(
        Offset(0f, 0f),
        Offset(10f, 0f),
        Offset(10f, 10f),
        Offset(0f, 10f),
        Offset(5f, 5f)
    )

    @Test
    fun runTest() {
        assertEquals(
            actual = testConvexHull.envelope.size,
            expected = 0
        )


        testConvexHull = ConvexHull(testPoints)

        assertNotEquals(
            actual = testConvexHull.envelope.size,
            illegal = testPoints.size
        )

        assertFalse(testConvexHull.envelope.contains(testPoints.last()))
    }
}