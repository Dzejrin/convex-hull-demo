import androidx.compose.material.MaterialTheme
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.runtime.Composable
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import gui.canvas.CanvasScreen

@Composable
@Preview
fun App() {
    MaterialTheme {
        CanvasScreen()
    }
}

fun main() = application {
    Window(
        onCloseRequest = ::exitApplication,
        title = "Convex hull"
    ) {
        App()
    }
}
