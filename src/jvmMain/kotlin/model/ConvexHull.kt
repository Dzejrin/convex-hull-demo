package model

import kotlin.math.atan2


/**
 * Wraps the logic that resolves unordered list of points into ordered list
 * called envelope that represents the convex hull.
 *
 * @property envelope ordered list representing the convex hull
 */
class ConvexHull(points: List<Point>) {

    val envelope: List<Point>

    /**
     * Sort the unordered list by x
     */
    private val orderedPoints = points.sortedBy { it.x }

    init {
        envelope = when (points.size) {
            in 0.until(2) ->
                emptyList()
            2 ->
                orderedPoints
            else ->
                findEnvelope()
        }
    }

    /**
     * Use orderedPoints list to find the envelope.
     * First create upperFence and lowerFence, then merge them into one list
     * omitting the points that are present in both list (first and last) and
     * reversing the lowerFence.
     *
     * @return the envelope
     */
    private fun findEnvelope(): List<Point> {
        val upperFence: List<Point>
        val lowerFence: List<Point>

        val createFence: (nextPoint: (current: Point) -> Point) -> List<Point> = { nextPoint ->
            arrayListOf<Point>().apply {
                var current = orderedPoints.first()
                add(current)

                while (current != orderedPoints.last()) {
                    current = nextPoint(current)
                    add(current)
                }
            }
        }

        upperFence = createFence { findNextUpperPoint(it) }
        lowerFence = createFence { findNextLowerPoint(it) }

        return buildList {
            addAll(upperFence)
            lowerFence.reversed().forEach {
                if (!contains(it)) {
                    add(it)
                }
            }
        }
    }

    /**
     * Returns point with higher index in orderedPoints and the highest angle from current.
     */
    private fun findNextUpperPoint(current: Point): Point {
        return findNextPoint(
            current = current,
            upper = true
        )
    }

    /**
     * Returns point with higher index in orderedPoints and the lowest angle from current.
     */
    private fun findNextLowerPoint(current: Point): Point {
        return findNextPoint(
            current = current,
            upper = false
        )
    }

    /**
     * Returns the next point for upper fence or lower fence.
     *
     * Create a nextPoints sub-list from orderedPoints skipping all the points
     * from first to current. Map nextPoints to angles using the angleTowards
     * function and find the target angle (max for upper, min for lower).
     * Next point is then found using the index of target angle.
     *
     * @param current point from which the next would be found
     * @param upper true/false whether the next point should be for upper/lower fence
     */
    private fun findNextPoint(current: Point, upper: Boolean): Point {
        val nextPoints = orderedPoints.subList(orderedPoints.indexOf(current) + 1, orderedPoints.size)

        if (nextPoints.isEmpty()) {
            return orderedPoints.last()
        }

        val angles = nextPoints.map { current.angleTowards(it) }
        val targetAngle = if (upper) angles.max() else angles.min()

        return nextPoints[angles.indexOf(targetAngle)]
    }
}