package model

import androidx.compose.ui.geometry.Offset
import kotlin.math.atan2


/**
 * Just for better readability
 */
typealias Point = Offset

fun Point.toShortString(): String {
    return "[$x, $y]"
}

/**
 * Returns angle of the vector between two points in degrees.
 */
fun Point.angleTowards(other: Point): Float {
    return Math.toDegrees(
        atan2(
            (other.x - x).toDouble(),
            (other.y - y).toDouble()
        )
    ).toFloat()
}