package gui.extensions

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import model.ConvexHull

/**
 * DrawScope's extension function that is used to draw ConvexHull object.
 */
fun DrawScope.drawConvexHull(convexHull: ConvexHull, color: Color) {
    convexHull.envelope.forEach { pointA ->
        val pointB = convexHull.envelope[(convexHull.envelope.indexOf(pointA) + 1) % convexHull.envelope.size]
        drawLine(
            color = color,
            start = pointA,
            end = pointB
        )
    }
}