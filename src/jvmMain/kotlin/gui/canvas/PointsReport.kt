package gui.canvas

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import model.ConvexHull
import model.Point
import model.toShortString

/**
 * Text box that shows all the points and its convex hull.
 */
@Composable
fun PointsReport(showReport: Boolean, points: List<Point>, convexHull: ConvexHull) {
    if (showReport) {
        Text(
            color = Color.Green,
            fontSize = 12.sp,
            modifier = Modifier
                .fillMaxWidth()
                .padding(2.dp)
                .background(Color.Black.copy(alpha = 0.9f))
                .padding(8.dp),
            text = buildString {
                append("All points: ")
                points.forEach {
                    append(it.toShortString())
                    if (it != points.last()) {
                        append(", ")
                    }
                }
                append(System.lineSeparator())
                append(System.lineSeparator())
                append("Convex hull: ")
                convexHull.envelope.forEach {
                    append(it.toShortString())
                    if (it != convexHull.envelope.last()) {
                        append(", ")
                    }
                }
            }
        )
    }
}