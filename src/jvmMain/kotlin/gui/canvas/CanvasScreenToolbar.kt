package gui.canvas

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

/**
 * Row with buttons to control basic functionality.
 * Includes: 1 Button for reset, 2 RadioButtons to toggle visibility of points & report
 */
@Composable
fun CanvasScreenToolbar(
    pointsVisible: Boolean,
    reportVisible: Boolean,
    reset: () -> Unit,
    showHidePoints: () -> Unit,
    reportPoints: () -> Unit,
    showHelp: () -> Unit
) {
    val buttonColors = ButtonDefaults.buttonColors(backgroundColor = Color.Gray)
    val radioButtonColors = RadioButtonDefaults.colors(selectedColor = Color.Green)

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.DarkGray)
    ) {
        Button(
            onClick = reset,
            colors = buttonColors,
            modifier = Modifier.padding(start = 6.dp)
        ) {
            Text(text = "🔄")
        }
        Box(
            modifier = Modifier
                .clickable {
                    showHidePoints()
                }
        ) {
            RadioButton(
                selected = pointsVisible,
                onClick = showHidePoints,
                colors = radioButtonColors
            )
            Text(
                text = "❎️",
                modifier = Modifier
                    .padding(start = 48.dp, end = 8.dp)
                    .align(Alignment.Center)
            )
        }
        Box(
            modifier = Modifier
                .clickable {
                    reportPoints()
                }
        ) {
            RadioButton(
                selected = reportVisible,
                onClick = reportPoints,
                colors = radioButtonColors
            )
            Text(
                text = "🧾",
                modifier = Modifier
                    .padding(start = 48.dp, end = 8.dp)
                    .align(Alignment.Center)
            )
        }
        Spacer(Modifier.weight(1f))
        Button(
            onClick = showHelp,
            colors = buttonColors,
            modifier = Modifier.padding(horizontal = 6.dp)
        ) {
            Text(text = "ℹ️")
        }
    }
}