package gui.canvas

import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput

/**
 * Composable that wraps all the visible GUI.
 */
@Composable
fun CanvasScreen() {
    val viewModel = remember { CanvasScreenViewModel() }
    val convexHull by viewModel.convexHull.collectAsState()
    val drawPoints by viewModel.showPoints.collectAsState()
    val showReport by viewModel.showReport.collectAsState()
    val showHelp by viewModel.showHelp.collectAsState()

    Box(Modifier.fillMaxSize()) {
        Column(Modifier.fillMaxSize()) {
            CanvasScreenToolbar(
                pointsVisible = drawPoints,
                reportVisible = showReport,
                reset = { viewModel.clearPoints() },
                showHidePoints = { viewModel.toggleShowPoints() },
                reportPoints = { viewModel.toggleShowReport() },
                showHelp = { viewModel.showHelp() }
            )

            Surface(
                modifier = Modifier
                    .weight(1f)
                    .pointerInput(Unit) {
                        detectTapGestures {
                            viewModel.addPoint(it)
                        }
                    }
            ) {
                ConvexHullCanvas(
                    points = viewModel.points,
                    convexHull = convexHull,
                    drawPoints = drawPoints
                )
            }

            PointsReport(
                showReport = showReport,
                points = viewModel.points,
                convexHull = convexHull
            )
        }

        HelpText(
            visible = showHelp,
            dismiss = { viewModel.dismissHelp() }
        )
    }
}

