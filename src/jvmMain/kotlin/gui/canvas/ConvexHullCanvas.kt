package gui.canvas

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Fill
import gui.extensions.drawConvexHull
import model.ConvexHull
import model.Point

/**
 * Composable that uses Canvas as a renderer to draw ConvexHull object
 * and a set of points.
 *
 * @param points set of points to be drawn
 * @param convexHull convex hull to be drawn
 * @param drawPoints when false points won't get drawn
 */
@Composable
fun ConvexHullCanvas(points: List<Point>, convexHull: ConvexHull, drawPoints: Boolean) {
    Canvas(Modifier.fillMaxSize()) {
        drawRect(
            color = Color.Black,
            topLeft = Offset(x = 0f, y = 0f),
            size = Size(10000f, 10000f),
            style = Fill
        )

        drawConvexHull(
            convexHull = convexHull,
            color = Color.LightGray
        )

        if (drawPoints) {
            points.forEach { point ->
                drawLine(
                    color = Color.Green,
                    start = Offset(x = point.x - 10f, y = point.y),
                    end = Offset(x = point.x + 10f, y = point.y)
                )
                drawLine(
                    color = Color.Green,
                    start = Offset(x = point.x, y = point.y - 10f),
                    end = Offset(x = point.x, y = point.y + 10f)
                )
            }
        }
    }
}