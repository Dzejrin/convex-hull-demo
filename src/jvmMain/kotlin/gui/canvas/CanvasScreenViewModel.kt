package gui.canvas

import androidx.compose.runtime.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import model.ConvexHull
import model.Point


class CanvasScreenViewModel {

    val convexHull: StateFlow<ConvexHull>
    val showPoints: StateFlow<Boolean>
    val showReport: StateFlow<Boolean>
    val showHelp: StateFlow<Boolean>
    val points: List<Point>
        get() = _points.toList()

    private var _convexHull = MutableStateFlow(ConvexHull(emptyList()))
    private var _showPoints = MutableStateFlow(true)
    private var _showReport = MutableStateFlow(true)
    private var _showHelp = MutableStateFlow(false)
    private var _points = mutableStateListOf<Point>()

    init {
        convexHull = _convexHull.asStateFlow()
        showPoints = _showPoints.asStateFlow()
        showReport = _showReport.asStateFlow()
        showHelp = _showHelp.asStateFlow()
    }

    /**
     * Record new input point
     */
    fun addPoint(point: Point) {
        if (_points.any { it.x == point.x && it.y == point.y }) {
            return
        }

        _points.add(point)
        _convexHull.value = ConvexHull(points.toList())
    }

    /**
     * Reset all input points
     */
    fun clearPoints() {
        _points.clear()
        _convexHull.value = ConvexHull(emptyList())
    }

    /**
     * Control state of points visibility
     */
    fun toggleShowPoints() {
        _showPoints.value = !_showPoints.value
    }

    /**
     * Control state of report visibility
     */
    fun toggleShowReport() {
        _showReport.value = !_showReport.value
    }

    /**
     *
     */
    fun showHelp() {
        _showHelp.value = true
    }

    /**
     *
     */
    fun dismissHelp() {
        _showHelp.value = false
    }
}