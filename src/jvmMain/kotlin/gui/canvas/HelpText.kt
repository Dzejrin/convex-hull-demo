package gui.canvas

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp


/**
 * Closable box with text showing how to control the app.
 */
@Composable
fun BoxScope.HelpText(visible: Boolean, dismiss: () -> Unit) {
    if (!visible) {
        return
    }

    Box(
        Modifier
            .background(Color.White.copy(alpha = 0.7f))
            .size(width = 280.dp, height = 160.dp)
            .align(Alignment.Center)
            .padding(12.dp)
    ) {
        Text(
            text = "✖️",
            modifier = Modifier
                .clickable { dismiss() }
                .padding(8.dp)
                .align(Alignment.TopEnd)
        )
        Column {
            Text(
                text = "Help",
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Text(
                text = " - click anywhere in the window to input points${System.lineSeparator()}" +
                        " - \"🔄\" button will reset the scene${System.lineSeparator()}" +
                        " - toggle \"❎\" to show/hide points${System.lineSeparator()}" +
                        " - toggle \"🧾\" to show/hide text report${System.lineSeparator()}" +
                        " - \"ℹ️\" button will show this help",
                fontSize = 12.sp,
                modifier = Modifier.padding(horizontal = 8.dp)
            )
        }
    }
}