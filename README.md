# Convex Hull demo

Simple project that implements convex hull algorithm and uses JetPack Compose Multiplatform framework for GUI.

## How to run it?

Import this project into your favourite Java IDE to build and run.

### IntelliJ IDEA

To import this project in IntelliJ IDEA go to *File* -> *Open* and select the root folder of this project. That's it!

#### Run the app

To run the app open file `Main.kt` and click the green triangle right next to the `main` function.

#### Run the unit test

There is also a simple unit test that checks if the algorithm works correctly. To run it, find file `ConvexHullTest.kt` and run the `runTest` function.

### How to control the running app?

There is just a very simple GUI to control this app:

- click anywhere in the window to input points
- "🔄" button will reset the scene
- toggle "❎" to show/hide points
- toggle "🧾" to show/hide text report
- "ℹ️" button will show help

## The algorithm

To find the convex hull we need to sort all the input points by their X coordinate and then find two paths from first to the last point. These paths are called `upperFence` and `lowerFence`. To find the upper fence we traverse the ordered list adding the first point and then keep adding points that form the highest angle with the currently added one until we add the last from the list. Lower fence works the same way only difference is that it looks for lowest angles. Once we have upper and lower fences we need to merge them into single list by adding the entire upper fence and then reversed lower fence omitting the points that are already added.

### Where to find it

It's in this file: `/src/jvmMain/model/ConvexHull.kt`